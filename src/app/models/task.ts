export interface Task {
  nombre: string;
  description: string;
  apellido: string;
  email: string;
  celular: string;
  fecha: string
  hora: string
  hide: boolean;

}
