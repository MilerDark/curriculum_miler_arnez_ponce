import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformacionDeContactoComponent } from './informacion-de-contacto.component';

describe('InformacionDeContactoComponent', () => {
  let component: InformacionDeContactoComponent;
  let fixture: ComponentFixture<InformacionDeContactoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformacionDeContactoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformacionDeContactoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
