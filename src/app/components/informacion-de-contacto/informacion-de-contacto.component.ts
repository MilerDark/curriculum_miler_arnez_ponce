import { HttpClient } from '@angular/common/http';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { i18nMetaToJSDoc } from '@angular/compiler/src/render3/view/i18n/meta';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import * as Notiflix from 'notiflix';

@Component({
  selector: 'app-informacion-de-contacto',
  templateUrl: './informacion-de-contacto.component.html',
  styleUrls: ['./informacion-de-contacto.component.css']
})
export class InformacionDeContactoComponent implements OnInit {
  datos:FormGroup;
  constructor(private httpclient: HttpClient,
              private fb: FormBuilder) {
                this.crearFormulario();
  }
    crearFormulario():void{
    this.datos = this.fb.group({
    nombre: ['', [Validators.required, Validators.pattern("[a-zA-Z ]{2,254}")]],
    email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
    asunto: ['',[Validators.required, Validators.minLength(3), Validators.maxLength(20),Validators.pattern("[a-zA-Z ]{2,254}")]],
    mensaje: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(120)]]
  });
  }
 
  get nombreNoValido(){
    return this.datos.get('nombre')?.invalid && this.datos.get('nombre')?.touched;
  }

  get emailNoValido(){
    return this.datos.get('email')?.invalid && this.datos.get('email')?.touched
  }


  get mensajeNoValido(){
    return this.datos.get('mensaje')?.invalid && this.datos.get('mensaje')?.touched
  }

  get asuntoNoValido(){
    return this.datos.get('asunto')?.invalid && this.datos.get('asunto')?.touched
  }
  ngOnInit(): void {
  }

  envioCorreo(){
    Notiflix.Loading.standard('Cargando...',{
    svgColor: 'rgba(0, 178, 255, 1)'
    });
    Notiflix.Loading.remove(2000);
    let params = {
      nombre:this.datos.value.nombre,
      email:this.datos.value.email,
      asunto:this.datos.value.asunto,
      mensaje:this.datos.value.mensaje
    }
    Notiflix.Notify.success('Enviado correctamente');
    this.datos.reset();
    console.log(params);
    
    // this.httpclient.post('http://localhost:3000/envio',params).subscribe(resp=>{
    //   console.log(resp);
    }

    // guardar():void{
    //   if(!this.datos.valid){
    //     return
    //   }
    //   this.limpiarForm();
    // }

    limpiarForm():void{
      this.datos.reset({

      })
    }
}
