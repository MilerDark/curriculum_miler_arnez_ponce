import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-curriculum-vitae',
  templateUrl: './curriculum-vitae.component.html',
  styleUrls: ['./curriculum-vitae.component.css']
})
export class CurriculumComponent implements OnInit {

  ngOnInit(): void {
    
  }

  mobileQuery: MediaQueryList;

  // fillerNav = Array.from({length: 50}, (_, i) => `Nav Item ${i + 1}`);
  fillerNav = [
    {name: 'INICIO', route:'inicio', icon:''},
    {name: 'DATOS GENERALES', route:'datos-generales', icon:''},
    {name: 'RESUMEN', route:'resumen', icon:''},
    {name: 'INFORMACIÓN DE CONTACTO', route:'informacion-de-contacto', icon:''},
    {name: 'AGENDA ELECTRÓNICA', route:'agenda-electronica', icon:''}
  ]

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }


}

  

