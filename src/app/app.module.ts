import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CurriculumComponent } from './components/curriculum-vitae/curriculum-vitae.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatosGeneralesComponent } from './components/datos-generales/datos-generales.component';
import { ResumenComponent } from './components/resumen/resumen.component';
import { InformacionDeContactoComponent } from './components/informacion-de-contacto/informacion-de-contacto.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';

import { MatGridListModule } from '@angular/material/grid-list';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule } from '@angular/forms'; 
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip'; 
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { TaskComponent } from './components/task/task.component';
import { TaskFormComponent } from './components/task-form/task-form.component';
import { TaskListComponent } from './components/task-list/task-list.component';
import { AgendaElectronicaComponent } from './components/agenda-electronica/agenda-electronica.component';
import { TaskService } from './services/task.service';
import { HttpClientModule } from '@angular/common/http';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';


import {MatNativeDateModule} from '@angular/material/core';




@NgModule({
  declarations: [
    AppComponent,
    CurriculumComponent,
    DatosGeneralesComponent,
    ResumenComponent,
    InformacionDeContactoComponent,
    TaskComponent,
    TaskFormComponent,
    TaskListComponent,
    AgendaElectronicaComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatProgressBarModule,
    MatGridListModule,
    MatSelectModule, 
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    MatIconModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatSnackBarModule,
    HttpClientModule,
    MatSidenavModule,
    MatListModule,
    MatNativeDateModule
   
    
  ],
  providers: [TaskService],
  bootstrap: [AppComponent]
})
export class AppModule { }
